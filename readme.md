###Index
* [Elektronik-Shops](#elektronik-shops)
* [Suchmaschinen](#suchmaschinen)
* [Nuetzliches](#nützliches)
* [Tutorials](#tutorials)



###Elektronik-Shops
* [Kessler Electronic](http://www.kessler-electronic.de/)



###Suchmaschinen
* [Koders.com](http://koders.com/)
* [searchcode.com](https://searchcode.com/)
* [sourcegraph](https://sourcegraph.com/)
* [codase](http://www.codase.com/)
* [grepcode](http://grepcode.com/)


###Nuetzliches
* [Elektronik Bastelkeller](http://www.elektronik-bastelkeller.de/)
* [Programmier das Ding!](http://www.sachsendreier.com/asw/clernen/proggenlernen.html)
* [Kriwanek](http://www.kriwanek.de/arduino.html)
* [Elektronik Kompendium](http://dse-faq.elektronik-kompendium.de/dse-faq.htm)

###Tutorials
* [Lasten mit Power MOSFET schalten](http://www.kriwanek.de/arduino/aktoren/304-lasten-mit-power-mosfet-schalten.html)